#!/usr/bin/env pybricks-micropython

import random

from pybricks import ev3brick as brick
from pybricks.ev3devices import Motor, TouchSensor
from pybricks.parameters import Port, Button, Color
from pybricks.tools import print, wait
from pybricks.robotics import DriveBase


class BumberRobot:
    def __init__(self):
        # Initialize both motors
        left_motor = Motor(Port.C)
        right_motor = Motor(Port.D)

        # Set the wheel diameter
        wheel_diameter = 56

        # Set the axle track (distance between the center of each wheel)
        axle_track = 200

        # Create a DriveBase instance (composed of two motors, with a wheel on each motor)
        self.driver = DriveBase(left_motor, right_motor,
                                wheel_diameter, axle_track)

        # Create a pair of touch sensors instances
        self.left_touch = TouchSensor(Port.S1)
        self.right_touch = TouchSensor(Port.S2)

    def go_around(self):
        # Start exploring
        while True:
            # Go somewhere
            self.driver.drive(750, 0)

            # If detect something
            if self.left_touch.pressed() or self.right_touch.pressed():
                # Turn around and pick a new direction
                self.driver.drive_time(-500, 0, 1000)
                self.driver.drive_time(250, 90, 2000)


# Make some noise when the program starts
brick.sound.beep()

# Create the robot instance and start going around
robot = BumberRobot()
robot.go_around()
