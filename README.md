# How to start playing #

## Requirements ##

* Mindstorm EV3 (and his cable)
* Micro SD (2GB or larger)
* A PC with Visual Code

## Steps ##

1. Download and flash the EV3 MicroPython image onto a micro SD card
2. Insert your micro SD card into the SD card slot on the EV3 Brick and turn it on
3. Download, install, and launch the free Visual Studio Code editor on your computer 
4. Install and activate the LEGO Education EV3 extension
5. Connect the EV3 Brick to your computer and start to code

Links to downloads and more info at:
<https://education.lego.com/en-us/support/mindstorms-ev3/python-for-ev3>

## Known limitations ##

* Only microSD or microSDHC cards are supported, no microSDXC
* The microSD must have at least 2GB
* mircoSD with more that 32GB are not supported
