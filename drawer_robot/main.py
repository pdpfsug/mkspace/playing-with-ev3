#!/usr/bin/env pybricks-micropython

import random

from pybricks import ev3brick as brick
from pybricks.ev3devices import Motor, ColorSensor
from pybricks.parameters import Port, Button, Color
from pybricks.tools import print, wait
from pybricks.robotics import DriveBase


class DrawerRobot:
    def __init__(self):
        # Initialize both motors
        left_motor = Motor(Port.D)
        right_motor = Motor(Port.A)

        # Set the wheel diameter
        wheel_diameter = 56

        # Set the axle track (distance between the center of each wheel)
        axle_track = 114

        # Create a DriveBase instance (composed of two motors, with a wheel on each motor)
        self.driver = DriveBase(left_motor, right_motor,
                                wheel_diameter, axle_track)

        # Create a ColorSensor instance
        self.color_sensor = ColorSensor(Port.S4)

    def print_info(self):
        # Clear the display
        brick.display.clear()

        # Print instructions in middle of the screen
        brick.display.text("LEFT -> Draw a circle", (0, 30))
        brick.display.text("RIGHT -> Random drawing")
        brick.display.text("UP -> Do both")

    def draw_circle(self):
        # Do the circle
        self.driver.drive_time(180, 90, 4000)

    def draw_random(self):
        # Turn right and pick a direction
        self.driver.drive_time(1, 90, 1000)
        direction = random.randint(0, 120)

        # Start exploring inside the circle
        while True:
            # Go somewhere
            self.driver.drive(200, direction)

            # Get the color
            color = self.color_sensor.color()

            # If color is different from white
            if color != 6:
                # Turn around and pick a new direction
                self.driver.drive_time(1, random.randint(140, 220), 1000)
                direction = random.randint(0, 120)

            # If can't see nothing -> STOP!
            if color == None:
                break


# Make some noise when the program starts
brick.sound.beep()

# Create an Instance of our drawer
drawer = DrawerRobot()

# Print some info
drawer.print_info()

# Wait for user input
while not any(brick.buttons()):
    wait(10)

# Check output
if Button.LEFT in brick.buttons():
    drawer.draw_circle()
elif Button.RIGHT in brick.buttons():
    drawer.draw_random()
elif Button.UP in brick.buttons():
    drawer.draw_circle()
    drawer.draw_random()
